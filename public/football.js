(function(){
    var FOOTBALLApp = angular.module("FOOTBALLApp", ["http-auth-interceptor", "ui.router", "GameModule"]);

    var LoginCtrl = function($http, $httpParamSerializerJQLike, authService) {
        var vm = this;
        vm.username = "";
        vm.password = "";
        vm.login = function() {
            $http({
                url: "/authenticate",
                method: "POST",
                data: $httpParamSerializerJQLike({
                    username: vm.username,
                    password: vm.password
                }),
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).then(function() {
                authService.loginConfirmed(vm.username);
            });
        }
    };
    
    var MenuCtrl = function($scope, $http, $templateCache, $state, authService) {
        var vm = this;
        vm.logout = function () {
            $http.get("/logout")
                .then(function() {
                    //ui.router caches views in $templateCache. Need to remove the cache page
                    //when we logout otherwise the cache page will be redisplayed
                    $templateCache.remove("/protected/views/menu.html");
                    //Fire an event to tell the ViewLoginCtrl that we have logout
                    $scope.$emit("event:auth-logout");
                    $state.go("list");
                });
        }
    }
    
    var FBListCtrl = function($http, $state, GameSvc) {
        var vm = this;
        vm.games=[];
        vm.displayDetails = function(game_id) {
            $state.go("details",
                { gameid: game_id});
        }
        var promise = GameSvc.getAllGames()
            .then(function(games) {
                vm.games = games

            }).catch(function(err) {
                console.error(">> %s", err)
            })
    };

    var DetailsCtrl = function ($http, $state, $stateParams, GameSvc){
        var vm = this;
        vm.game = {};
        vm.back = function (){
            $state.go("list");
        }
        // console.info(">> game = " + $stateParams.gameid)
        var promise = GameSvc.getGame($stateParams.gameid)
            .then(function(game) {
                vm.game = game

            }).catch(function(err) {
                console.error(">> %s", err)
            })
    };
    
    var FBOrganiseCtrl = function($http, $state) {
        var vm = this;
        vm.venues=[];   
        $http.get("/organise")
            .then(function(venue){
                console.info(JSON.stringify(venue.data))
                vm.venues = venue.data;
            })
    };
    
    var FOOTBALLConfig = function($httpProvider, $stateProvider, $urlRouterProvider) {
        $httpProvider.defaults.withCredentials = true;
        $stateProvider.state("list", {
                url: "/list",
                templateUrl: "/views/list.html",
                controller: ["$http", "$state", "GameSvc", FBListCtrl],
                controllerAs: "fblistCtrl"

            }).state("login", {
                url: "/login",
                templateUrl: "/views/login.html",
                controller: ["$http", "$httpParamSerializerJQLike", "authService", LoginCtrl],
                controllerAs: "loginCtrl"
            
            }).state("protected_menu", {
                url: "/protected/menu",
                templateUrl: "/protected/views/menu.html",
                controller: ["$scope", "$http", "$templateCache", "$state", "authService", MenuCtrl],
                controllerAs: "menuCtrl"

            }).state("details",{
                url:"/game/:gameid",
                templateUrl:"/views/details.html",
                controller: ["$http", "$state", "$stateParams","GameSvc", DetailsCtrl],
                controllerAs:"detailsCtrl"

            }).state("organise", {
                url: "/organise",
                templateUrl: "/views/organise.html",
                controller: ["$http", "$state", FBOrganiseCtrl],
                controllerAs: "fborganiseCtrl"
        });
        $urlRouterProvider.otherwise("/list");
    };
    
    var ViewsLoginCtrl = function($scope, $state, authService) {

        var vm = this;

        vm.status = {
            message: ""
        }

        //401
        $scope.$on("event:auth-loginRequired", function() {
            vm.status.message = "Please login";
            $state.go("login");
        });
        //200
        $scope.$on("event:auth-loginConfirmed", function(_, name) {
            vm.status.message = "Hello " + name;
            $state.go("protected_menu");
        });
        //403
        $scope.$on("event:auth-forbidden", function() {
            vm.status.message = "Please username/password. Please try again";
            authService.loginConfirmed();
        });
        $scope.$on("event:auth-loginCancelled", function() {

        });

        $scope.$on("event:auth-logout", function() {
            vm.status.message = "";
        })
    }
    
    FOOTBALLApp.config(["$httpProvider", "$stateProvider", "$urlRouterProvider", FOOTBALLConfig]);
    FOOTBALLApp.controller("ViewsLoginCtrl", ["$scope", "$state", "authService", ViewsLoginCtrl]);
})();

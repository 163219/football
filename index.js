var express = require("express");
var app = express();

var path = require("path");

var bodyParser = require("body-parser");

var session = require("express-session");

const uploadDir = path.join(__dirname, "tmp/");
var multer = require("multer");
var uploader = multer({
    dest: uploadDir
});

var mysql = require("mysql");
const connPool = mysql.createPool({
    host: "127.0.0.1",
    user: "root",
    password: "ttc",
    database: "football",
    connectionLimit: 5
});

var asset = require("./module/asset_management")(connPool);

// Passport
var ensure = require("connect-ensure-login");
var passport = require("passport");
var PassportLocal = require("passport-local");
var local = new PassportLocal(
    { usernameField: "username", passwordField: "password" },
    function(username, password, done) {
        if (username == password)
            return (done(null, username));
        return (done(null, false));
    });

passport.use(local);
passport.serializeUser(function(user, done) {
    done(null, user);
});
passport.deserializeUser(function(id, done) {
    done(null, {username: id});
});

app.use(session({
    secret: "hello",
    resave: false,
    saveUninitialized: false
}));

app.use(bodyParser.urlencoded({ extended: false }));

app.use(passport.initialize());
app.use(passport.session());

app.use("/protected/*", ensure.ensureLoggedIn("/status/401"));

app.post("/authenticate", passport.authenticate("local", {
    successRedirect: "/status/200",
    failureReturnToOrRedirect: "/status/403"
}));


app.get("/status/:code", function(req, res) {
    //res.status(parseInt(req.params.code)).send();
    res.sendStatus(parseInt(req.params.code));
});

app.get("/logout", function(req, res) {
    req.logout();
    req.session.destroy();
    res.sendStatus(200);
})
// end Passport

const q = require("q");

var listGames = function(connPool) {
    var defer = q.defer();
    connPool.getConnection(function(err, conn) {
        if (err) {
            defer.reject(err);
            return;
        }
        try {
            conn.query("SELECT * FROM football.games AS ga LEFT JOIN football.venues AS ve ON ga.venue_id=ve.venue_id LIMIT 0, 1000",
                function(err, rows) {
                    if (err) {
                        defer.reject(err);
                        return;
                    }
                    defer.resolve(rows);
                    console.info("connected to DB")
                });
        } catch (ex) { defer.reject(ex); }
        finally { conn.release(); }
    });
    return (defer.promise);
};

var listVenues = function(connPool) {
    var defer = q.defer();
    connPool.getConnection(function(err, conn) {
        if (err) {
            defer.reject(err);
            return;
        }
        try {
            conn.query("select * from football.venues",
                function(err, rows) {
                    if (err) {
                        defer.reject(err);
                        return;
                    }
                    defer.resolve(rows);
                    console.info("connected to venue DB")
                });
        } catch (ex) { defer.reject(ex); }
        finally { conn.release(); }
    });
    return (defer.promise);
};

var getGame = function(connPool, game_id) {
    var defer = q.defer();
    connPool.getConnection(function(err, conn) {
        if (err) {
            defer.reject(err);
            return;
        }
        try {
            conn.query("SELECT * FROM football.games AS ga LEFT JOIN football.venues AS ve ON ga.venue_id=ve.venue_id where game_id= ? LIMIT 0, 1000", [game_id],
                function(err, rows) {
                    if (err) {
                        defer.reject(err);
                        return;
                    }
                    if (rows.length)
                    defer.resolve(rows [0])
                    else
                    defer.reject (404);
                });
        } catch (ex) {
            defer.reject(ex);
        } finally { conn.release()
        }
    });
    return (defer.promise);
}.bind(undefined, connPool);

app.get("/game/:game_id", function(req, res) {
    getGame(req.params.game_id)
        .then(function(rows) {
            res.json(rows);
        }).catch(function(err) {
        if (err == 404) {
            res.status(404);
            res.type("text/plain");
            res.send("Not found");
        } else {
            res.status(400);
            res.type("text/plain");
            res.send(err);
        }
    })
});

app.get("/list", function(req, res) {
    listGames(connPool)
        .then(function(rows) {
            //defer.resolve
            //res.status(200);
            //res.type("application/json");
            //res.send(rows);
            res.json(rows);
        }).catch(function(err) {
        //defer.reject
        res.status(400);
        res.type("text/plain");
        res.send("error: " + err);
    });
});

app.get("/organise",function(req, res) {
    console.info(">>> in organise")
    listVenues(connPool)
        .then(function(rows) {
            //defer.resolve
            //res.status(200);
            //res.type("application/json");
            //res.send(rows);
            res.json(rows);
            // console.info("venues dropdown" + JSON.stringify(rows))
        }).catch(function(err) {
        //defer.reject
        res.status(400);
        res.type("text/plain");
        res.send("error: " + err);
    });
});

app.post("/org", function(req, res) {
    connPool.getConnection(function (err, conn) {
        if (err) {
            console.error("get connection error: %s", err);
            res.redirect("/list");
            return;
        }

        try {
            conn.query("INSERT INTO `football`.`games` (`venue_id`, `pitch_type`, `pitch_info`, `date`, `start_time`, `end_time`, `min_player_cnt`, `max_player_cnt`, `price_per_booking` ) values (?, ?, ?, ? ,? ,? ,?, ?, ?)",
                [req.body.venue_id, req.body.pitch_type, req.body.pitch_info, req.body.date, req.body.start_time, req.body.end_time, req.body.min_player_cnt, req.body.max_player_cnt, req.body.price_per_booking],
                //actual parameters based on position
                function (err, result) {
                    if (err){
                        console.info("error:" + JSON.stringify(err))
                    }
                    conn.release();
                    console.info("inserted into games table");
                    res.redirect("/#list");
                });
        }
        catch (e) {
        } finally {
        }
    });
});

app.use("/bower_components", express.static(path.join(__dirname, "bower_components")));
app.use(express.static(path.join(__dirname, "public")));

//Handle file not found - 400
app.use(function(req, res, next) {
    res.status(404);
    res.type("text/plain");
    res.send("File not found: " + req.originalUrl);
});

//Error - 500
app.use(function(err, req, res, next) {
    console.error("Application error: %s", err);
});

app.set("port", process.env.APP_PORT || 3000);
app.listen(app.get("port"), function() {
    console.info("Application started on port %d", app.get("port"));
});
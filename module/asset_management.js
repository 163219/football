var fs = require("fs");
var q = require("q");

var AssetManagement = function(connPool) {
    this.connPool = connPool;
};

AssetManagement.prototype.insertImage = function(file) {
    const sql = "insert into asset(asset_id, image, name, size, image_type) values (?, ?, ?, ?, ?)";
    var defer = q.defer();
    this.connPool.getConnection(function(err, connection) {
        if (err) {
            defer.reject(JSON.stringify(err));
            return;
        }

        //Read the file and insert it into the database as blog
        fs.readFile(file.path, function(err, data) {
            connection.query(sql, [file.filename, data, file.originalname, file.size, file.mimetype],
                function(err, result) {
                    connection.release();
                    if (err) {
                        defer.reject(JSON.stringify(err));
                        return;
                    }
                    //Don't keep the file uploaded file 
                    fs.unlink(file.path);
                    defer.resolve(file.filename);
                });
        })
    });
    return (defer.promise);
};

AssetManagement.prototype.getImages = function() {
    
    var defer = q.defer();
    
    this.connPool.getConnection(function(err, conn) {
        if (err) {
            defer.reject(JSON.stringify(err));
            return;
        }

        conn.query("select asset_id, name from asset", function(err, result) {
            conn.release();
            if (err) {
                defer.reject(JSON.stringify(err));
                return;
            }
            defer.resolve(result);
        });

    });
    return (defer.promise);
}

AssetManagement.prototype.getImage = function(assetId) {
    var defer = q.defer();
    this.connPool.getConnection(function(err, conn) {
        if (err) {
            defer.reject(JSON.stringify(err));
            return;
        }
        conn.query("select image,image_type from asset where asset_id = ?", [assetId], 
            function(err, result) {
                if (err)
                    defer.reject(JSON.stringify(err));
                else
                    defer.resolve(result[0]);
                conn.release();
            });
    });
    return (defer.promise);
}

module.exports = function(connPool) {
    return (new AssetManagement(connPool));
};